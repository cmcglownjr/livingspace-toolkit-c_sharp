using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Serilog;

namespace LivingspaceToolkitLib
{
    public class Studio:Sunroom, IStudio
    {
        public double pitch, peak, max_h, unpitched_wall, soffit, drip_edge, pitched_wall, soffit_wall;
        public Dictionary<string, object> panel_length_dict, roof_panel_dict, hang_rail_dict, fascia_dict = new Dictionary<string, object>();
        public double armstrong_panels;
        public Studio(double pOverhang, double pAwall, double pBwall, double pCwall, double pThickness, string pEndcut): base(pOverhang, pAwall, pBwall, pCwall, pThickness, pEndcut)
        {
            pitched_wall = Math.Max(this.awall, this.cwall);
            soffit_wall = this.bwall;
        }
        public virtual Dictionary<string, object> calculate_roof_panels(double soffit_wall, Dictionary<string, object> panel_length_dict)
        {
            double roof_area;
            List<bool> minmax_overhang = new List<bool>() {false, false};
            Dictionary<string, object> temp = new Dictionary<string, object>();
            double roof_width = soffit_wall + side_overhang * 2;
            double roof_panels = Math.Ceiling(roof_width / 32);
            if ((roof_panels * 32 - soffit_wall) / 2 < side_overhang)
            {
                side_overhang = (roof_panels * 32 - soffit_wall) / 2;
                minmax_overhang[0] = true;
            }
            else if ((roof_panels * 32 - soffit_wall) / 2 > 16)
            {
                side_overhang = (roof_panels * 32 - soffit_wall) / 2;
                minmax_overhang[1] = true;
            }
            if ((bool)panel_length_dict["Max Length Check"])
            {
                roof_area = (double)panel_length_dict["Panel Length"] * 2 * roof_panels * 32;
            }
            else
            {
                roof_area = (double)panel_length_dict["Panel Length"] * roof_panels * 32;
            }
            temp.Add("Roof Area", roof_area);
            temp.Add("Roof Panels", roof_panels);
            temp.Add("Side Overhang", side_overhang);
            temp.Add("Overhang Short Check", minmax_overhang[0]);
            temp.Add("Overhang Long Check", minmax_overhang[1]);
            return temp;
        }
        public virtual Dictionary<string, object> calculate_hang_rail(Dictionary<string, object> panel_dict)
        {
            Dictionary<string, object> temp = new Dictionary<string, object>();
            bool max_hang_rail_length = false;
            double hang_rail = (double)panel_dict["Roof Panels"] * 32;
            if (hang_rail > 216)
            {
                max_hang_rail_length = true;
                hang_rail /= 2;
            }
            temp.Add("Hang Rail", hang_rail);
            temp.Add("Hang Rail Check", max_hang_rail_length);
            return temp;
        }
        public virtual Dictionary<string, object> calculate_fascia(Dictionary<string, object> roof_panel_dict, Dictionary<string, object> panel_length_dict)
        {
            Dictionary<string, object> temp = new Dictionary<string, object>();
            List<bool> max_fascia_length = new List<bool>() {false, false};
            double fascia_wall = (double)roof_panel_dict["Roof Panels"] * 32 + 12;
            double fascia_sides = (double)panel_length_dict["Panel Length"] + 6;
            if (fascia_wall > 216)
            {
                max_fascia_length[0] = true;
                fascia_wall /= 2;
            }
            if (fascia_sides > 216)
            {
                max_fascia_length[1] = true;
                fascia_sides /= 2;
            }
            temp.Add("Wall Fascia", fascia_wall);
            temp.Add("Side Fascia", fascia_sides);
            temp.Add("Fascia Check", max_fascia_length);
            return temp;
        }
        protected override void calculate_sunroom()
        {
            panel_length_dict = calculate_panel_length(pitch, pitched_wall);
            roof_panel_dict = calculate_roof_panels(soffit_wall, panel_length_dict);
            hang_rail_dict = calculate_hang_rail(roof_panel_dict);
            fascia_dict = calculate_fascia(roof_panel_dict, panel_length_dict);
            armstrong_panels = Math.Ceiling(Methods.calculate_armstrong_panels(pitch, pitched_wall, soffit_wall));
        }
        public void wall_height_pitch(double pPitch, double pSoffit_wall_height)
        {
            pitch = pPitch;
            unpitched_wall = pSoffit_wall_height;
            soffit = unpitched_wall - overhang * Math.Tan(pitch);
            peak = unpitched_wall + pitched_wall * Math.Tan(pitch);
            max_h = peak + Methods.angled(pitch, thickness);
            drip_edge = calculate_drip_edge(soffit, pitch);
            calculate_sunroom();
        }
        public void wall_height_peak_height(double pSoffit_wall_height, double pPeak)
        {
            unpitched_wall = pSoffit_wall_height;
            peak = pPeak;
            pitch = Math.Atan((peak - unpitched_wall)/ pitched_wall);
            soffit = unpitched_wall - overhang * Math.Tan(pitch);
            max_h = peak + Methods.angled(pitch, thickness);
            drip_edge = calculate_drip_edge(soffit, pitch);
            calculate_sunroom();
        }
        public void max_height_pitch(double pPitch, double pMax_h)
        {
            pitch = pPitch;
            max_h = pMax_h;
            unpitched_wall = max_h - pitched_wall * Math.Tan(pitch) - Methods.angled(pitch, thickness);
            soffit = unpitched_wall - overhang * Math.Tan(pitch);
            peak = max_h - Methods.angled(pitch, thickness);
            drip_edge = calculate_drip_edge(soffit, pitch);
            calculate_sunroom();
        }
        public void soffit_height_peak_height(double pSoffit_height, double pPeak)
        {
            soffit = pSoffit_height;
            peak = pPeak;
            pitch = Math.Atan((peak - soffit)/(pitched_wall + overhang));
            unpitched_wall = soffit + overhang * Math.Tan(pitch);
            max_h = peak + Methods.angled(pitch, thickness);
            drip_edge = calculate_drip_edge(soffit, pitch);
            calculate_sunroom();
        }
        public void soffit_height_pitch(double pPitch, double pSoffit_height)
        {
            pitch = pPitch;
            soffit = pSoffit_height;
            unpitched_wall = soffit + overhang * Math.Tan(pitch);
            peak = unpitched_wall + pitched_wall * Math.Tan(pitch);
            max_h = peak + Methods.angled(pitch, thickness);
            drip_edge = calculate_drip_edge(soffit, pitch);
            calculate_sunroom();
        }
        public void drip_edge_peak_height(double pDrip_edge, double pPeak)
        {
            peak = pPeak;
            drip_edge = pDrip_edge;
            double tol = 0.01;
            double diff = 100;
            double incr = 0.1;
            double ratio_pitch = 0.0;
            double old_ratio, drip_est;
            while (diff > tol)
            {
                old_ratio = ratio_pitch;
                ratio_pitch += incr;
                pitch = Math.Atan2(ratio_pitch, 12.0);
                drip_est = Methods.estimate_drip_from_peak(peak, pitch, pitched_wall, overhang, thickness, awall, bwall, cwall, endcut);
                diff = Math.Abs(drip_edge - drip_est);
                if (ratio_pitch > 12) {break;}
                if (drip_est < drip_edge)
                {
                    ratio_pitch = old_ratio;
                    incr /= 2;
                }
            }
            unpitched_wall = peak - pitched_wall * Math.Tan(pitch);
            soffit = unpitched_wall - overhang * Math.Tan(pitch);
            max_h = peak + Methods.angled(pitch, thickness);
            drip_edge = calculate_drip_edge(soffit, pitch);
            calculate_sunroom();
        }
        public void drip_edge_pitch(double pDrip_edge, double pPitch)
        {
            pitch = pPitch;
            drip_edge = pDrip_edge;
            soffit = drip_edge - Methods.angled(pitch, thickness);
            unpitched_wall = soffit + overhang * Math.Tan(pitch);
            peak = unpitched_wall + pitched_wall * Math.Tan(pitch);
            max_h = peak + Methods.angled(pitch, thickness);
            drip_edge =calculate_drip_edge(soffit, pitch);
            calculate_sunroom();
        }
    }
}