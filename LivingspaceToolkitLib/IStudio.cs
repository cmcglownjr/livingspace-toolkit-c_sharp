using System;
using System.Collections.Generic;

namespace LivingspaceToolkitLib
{
    public interface IStudio: ISunroom
    {
    void wall_height_pitch(double pPitch, double pSoffit_wall_height);
    void wall_height_peak_height(double pSoffit_wall_height, double pPeak);
    void max_height_pitch(double pPitch, double pMax_h);
    void soffit_height_peak_height(double pSoffit_height, double pPeak);
    void soffit_height_pitch(double pPitch, double pSoffit_height);
    void drip_edge_peak_height(double pDrip_edge, double pPeak);
    void drip_edge_pitch(double pDrip_edge, double pPitch);
    }
}