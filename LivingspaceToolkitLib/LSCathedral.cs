using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Serilog;

namespace LivingspaceToolkitLib
{
    public class Cathedral:Sunroom, ICathedral
    {
        public double a_pitch, c_pitch, peak, max_h, a_unpitched_wall, c_unpitched_wall, a_soffit, c_soffit, a_drip_edge, c_drip_edge, 
                    pitched_wall, a_unpitched_wall_h, c_unpitched_wall_h, f_peak, a_pitched_wall, c_pitched_wall;
        double post_width = 3.25;
        public Dictionary<string, object> a_panel_length_dict, a_roof_panel_dict, a_hang_rail_dict, a_fascia_dict = new Dictionary<string, object>();
        public Dictionary<string, object> c_panel_length_dict, c_roof_panel_dict, c_hang_rail_dict, c_fascia_dict = new Dictionary<string, object>();
        public double a_armstrong_panels, c_armstrong_panels;
        public Cathedral(double pOverhang, double pAwall, double pBwall, double pCwall, double pThickness, string pEndcut): base(pOverhang, pAwall, pBwall, pCwall, pThickness, pEndcut)
        {
            pitched_wall = this.bwall;
        }
        public Dictionary<string, object> calculate_roof_panels(double soffit_wall, Dictionary<string, object> panel_length_dict)
        {
            List<bool> minmax_overhang = new List<bool>() {false, false};
            Dictionary<string, object> temp = new Dictionary<string, object>();
            double roof_panels, roof_width, roof_area;
            bool split = false;
            roof_width = soffit_wall + side_overhang;
            if ((roof_width / 32) == (Math.Floor(roof_width / 32))) {roof_panels = Math.Floor(roof_width / 32);}
            else if ((roof_width / 32) >= (Math.Floor(roof_width / 32) + 0.5))
            {
                roof_panels = Math.Floor(roof_width / 32) + 0.5;
                split = true;
            }
            else
            {
                roof_panels = Math.Ceiling(roof_width / 32);
            }
            if ((roof_panels * 32 - soffit_wall) < side_overhang)
            {
                // Overhang too short
                side_overhang = roof_panels * 32 - soffit_wall;
                minmax_overhang[0] = true;
            }
            else if ((roof_panels * 32 - soffit_wall) > 16)
            {
                // Overhang too long
                side_overhang = roof_panels * 32 - soffit_wall;
                minmax_overhang[1] = true;
            }
            if ((bool)panel_length_dict["Max Length Check"]) 
            {roof_area = Math.Ceiling((double)panel_length_dict["Panel Length"] * 2 * roof_panels* 32);}
            else 
            {roof_area = Math.Ceiling((double)panel_length_dict["Panel Length"] * roof_panels* 32);}
            temp.Add("Roof Area", roof_area);
            temp.Add("Roof Panels", roof_panels);
            temp.Add("Side Overhang", side_overhang);
            temp.Add("Overhang Short Check", minmax_overhang[0]);
            temp.Add("Overhang Long Check", minmax_overhang[1]);
            temp.Add("Split", split);
            return temp;
        }
        public Dictionary<string, object> calculate_hang_rail(Dictionary<string, object> panel_dict)
        {
            Dictionary<string, object> temp = new Dictionary<string, object>();
            bool max_hang_rail_length = false;
            double hang_rail = (double)panel_dict["Panel Length"];
            if (hang_rail > 216)
            { 
                max_hang_rail_length = true;
                hang_rail /= 2;
            }
            temp.Add("Hang Rail", hang_rail);
            temp.Add("Hang Rail Check", max_hang_rail_length);
            return temp;
        }
        public Dictionary<string, object> calculate_fascia(Dictionary<string, object> roof_panel_dict, Dictionary<string, object> panel_length_dict)
        {
            Dictionary<string, object> temp = new Dictionary<string, object>();
            List<bool> max_fascia_length = new List<bool>() {false, false};
            double fascia_wall = (double)roof_panel_dict["Roof Panels"] * 32 + 6;
            double fascia_sides = (double)panel_length_dict["Panel Length"] + 6;
            if (fascia_wall > 216)
            {
                max_fascia_length[0] = true;
                fascia_wall /= 2;
            }
            if (fascia_sides > 216)
            {
                max_fascia_length[1] = true;
                fascia_sides /= 2;
            }
            temp.Add("Wall Fascia", fascia_wall);
            temp.Add("Side Fascia", fascia_sides);
            temp.Add("Fascia Check", max_fascia_length);
            return temp;
        }
        protected override void calculate_sunroom()
        {
            a_panel_length_dict = calculate_panel_length(a_pitch, a_pitched_wall);
            c_panel_length_dict = calculate_panel_length(c_pitch, c_pitched_wall);
            a_roof_panel_dict = calculate_roof_panels(awall, a_panel_length_dict);
            c_roof_panel_dict = calculate_roof_panels(cwall, c_panel_length_dict);
            a_hang_rail_dict = calculate_hang_rail(a_panel_length_dict);
            c_hang_rail_dict = calculate_hang_rail(c_panel_length_dict);
            a_fascia_dict = calculate_fascia(a_roof_panel_dict, a_panel_length_dict);
            c_fascia_dict = calculate_fascia(c_roof_panel_dict, c_panel_length_dict);
            a_armstrong_panels = Math.Ceiling(Methods.calculate_armstrong_panels(a_pitch, a_pitched_wall, awall));
            c_armstrong_panels = Math.Ceiling(Methods.calculate_armstrong_panels(c_pitch, c_pitched_wall, cwall));
        }
        public void wall_height_pitch(List<double> pPitch, List<double> pSoffit_wall_height)
        {
            a_pitch = pPitch[0];
            c_pitch = pPitch[1];
            a_unpitched_wall_h = pSoffit_wall_height[0];
            c_unpitched_wall_h = pSoffit_wall_height[1];
            a_pitched_wall = bwall / 2;
            c_pitched_wall = bwall / 2;
            a_soffit = a_unpitched_wall_h - overhang * Math.Tan(a_pitch);
            c_soffit = c_unpitched_wall_h - overhang * Math.Tan(c_pitch);
            peak = (pitched_wall * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / 
                    Math.Sin(Math.PI - a_pitch - c_pitch) + Math.Max(a_unpitched_wall_h, c_unpitched_wall_h);
            a_unpitched_wall = (peak - Math.Max(a_unpitched_wall_h, c_unpitched_wall_h)) / Math.Tan(a_pitch);
            c_unpitched_wall = (peak - Math.Max(a_unpitched_wall_h, c_unpitched_wall_h)) / Math.Tan(c_pitch);
            f_peak = peak - (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            max_h = f_peak + Math.Max(Methods.angled(a_pitch, thickness), Methods.angled(c_pitch, thickness)) + 
                    (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            a_drip_edge = calculate_drip_edge(a_soffit, a_pitch);
            c_drip_edge = calculate_drip_edge(c_soffit, c_pitch);
            calculate_sunroom();
        }
        public void wall_height_peak_height(List<double> pSoffit_wall_height, double pPeak)
        {
            f_peak = pPeak;
            a_unpitched_wall_h = pSoffit_wall_height[0];
            c_unpitched_wall_h = pSoffit_wall_height[1];
            a_pitched_wall = bwall / 2;
            c_pitched_wall = bwall / 2;
            a_pitch = Math.Atan2((f_peak - a_unpitched_wall_h), (a_pitched_wall - post_width / 2));
            c_pitch = Math.Atan2((f_peak - c_unpitched_wall_h), (c_pitched_wall - post_width / 2));
            a_soffit = a_unpitched_wall_h - overhang * Math.Tan(a_pitch);
            c_soffit = c_unpitched_wall_h - overhang * Math.Tan(c_pitch);
            double a_max_h = f_peak + Methods.angled(a_pitch, thickness) + 
                    (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            double c_max_h = f_peak + Methods.angled(c_pitch, thickness) + 
                    (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            max_h = Math.Max(a_max_h, c_max_h);
            a_drip_edge = calculate_drip_edge(a_soffit, a_pitch);
            c_drip_edge = calculate_drip_edge(c_soffit, c_pitch);
            calculate_sunroom();
        }
        public void max_height_pitch(List<double> pPitch, double pMax_h)
        {
            a_pitch = pPitch[0];
            c_pitch = pPitch[1];
            max_h = pMax_h;
            peak = (bwall * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            f_peak = max_h - Math.Max(Methods.angled(a_pitch, thickness), Methods.angled(c_pitch, thickness)) - 
                    (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            a_pitched_wall = peak / Math.Tan(a_pitch);
            c_pitched_wall = peak / Math.Tan(c_pitch);
            a_unpitched_wall_h = max_h - Math.Max(Methods.angled(a_pitch, thickness), Methods.angled(c_pitch, thickness)) - 
                    (bwall * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            c_unpitched_wall_h = a_unpitched_wall_h;
            a_soffit = a_unpitched_wall_h - overhang * Math.Tan(a_pitch);
            c_soffit = a_unpitched_wall_h - overhang * Math.Tan(c_pitch);
            a_drip_edge = calculate_drip_edge(a_soffit, a_pitch);
            c_drip_edge = calculate_drip_edge(c_soffit, c_pitch);
            calculate_sunroom();
        }
        public void soffit_height_peak_height(List<double> pSoffit_height, double pPeak)
        {
            a_soffit = Math.Max(pSoffit_height[0], pSoffit_height[1]);
            c_soffit = Math.Max(pSoffit_height[0], pSoffit_height[1]);
            f_peak = pPeak;
            a_pitched_wall = bwall / 2;
            c_pitched_wall = bwall / 2;
            a_pitch = Math.Atan((f_peak - a_soffit) / (a_pitched_wall + overhang - post_width / 2));
            c_pitch = Math.Atan((f_peak - c_soffit) / (c_pitched_wall + overhang - post_width / 2));
            a_unpitched_wall_h = a_soffit + overhang * Math.Tan(a_pitch);
            c_unpitched_wall_h = c_soffit + overhang * Math.Tan(c_pitch);
            double h = (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            max_h = Math.Max(f_peak + Methods.angled(a_pitch, thickness) + h, f_peak + Methods.angled(c_pitch, thickness) + h);
            a_drip_edge = calculate_drip_edge(a_soffit, a_pitch);
            c_drip_edge = calculate_drip_edge(c_soffit, c_pitch);
            calculate_sunroom();
        }
        public void soffit_height_pitch(List<double> pSoffit_height, List<double> pPitch)
        {
            a_soffit = Math.Max(pSoffit_height[0], pSoffit_height[1]);
            c_soffit = Math.Max(pSoffit_height[0], pSoffit_height[1]);
            a_pitch = pPitch[0];
            c_pitch = pPitch[1];
            a_unpitched_wall_h = a_soffit + overhang * Math.Tan(a_pitch);
            c_unpitched_wall_h = c_soffit + overhang * Math.Tan(c_pitch);
            peak = (bwall * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch) + 
                    Math.Max(a_unpitched_wall_h, c_unpitched_wall_h);
            a_pitched_wall = (peak - Math.Max(a_unpitched_wall_h, c_unpitched_wall_h)) / Math.Tan(a_pitch);
            c_pitched_wall = (peak - Math.Max(a_unpitched_wall_h, c_unpitched_wall_h)) / Math.Tan(c_pitch);
            f_peak = peak - (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            max_h = f_peak + Math.Max(Methods.angled(a_pitch, thickness), Methods.angled(c_pitch, thickness)) + 
                    (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            a_drip_edge = calculate_drip_edge(a_soffit, a_pitch);
            c_drip_edge = calculate_drip_edge(c_soffit, c_pitch);
            calculate_sunroom();
        }
        public void drip_edge_peak_height(double pDrip_edge, double pPeak)
        {
            a_drip_edge = pDrip_edge;
            c_drip_edge = pDrip_edge;
            f_peak = pPeak;
            double tol = 0.01;
            double diff = 100;
            double incr = 0.1;
            double ratio_pitch = 0.0;
            double pitch = 0;
            double old_ratio, drip_est;
            while (diff > tol)
            {
                old_ratio = ratio_pitch;
                ratio_pitch += incr;
                pitch = Math.Atan2(ratio_pitch, 12);
                drip_est = Methods.estimate_drip_from_peak(f_peak, pitch, bwall/2 - post_width/2, overhang, thickness, awall, bwall, cwall, endcut);
                diff = Math.Abs(pDrip_edge - drip_est);
                if (ratio_pitch > 12) {break;}
                if (drip_est < pDrip_edge)
                {
                    ratio_pitch = old_ratio;
                    incr /= 2;
                }
            }
            a_unpitched_wall_h = f_peak - (bwall / 2 - post_width / 2) * Math.Tan(pitch);
            c_unpitched_wall_h = a_unpitched_wall_h;
            a_pitched_wall = bwall / 2;
            c_pitched_wall = bwall / 2;
            a_soffit = a_unpitched_wall_h - overhang * Math.Tan(pitch);
            c_soffit = c_unpitched_wall_h - overhang * Math.Tan(pitch);
            a_pitch = pitch;
            c_pitch = pitch;
            max_h = f_peak + Methods.angled(pitch, thickness) + (post_width * Math.Sin(pitch) * Math.Sin(pitch)) / Math.Sin(Math.PI - pitch - pitch);
            calculate_sunroom();
        }
        public void drip_edge_pitch(double pDrip_edge, List<double> pPitch)
        {
            a_pitch = pPitch[0];
            c_pitch = pPitch[1];
            a_soffit = pDrip_edge - Methods.angled(a_pitch, thickness);
            c_soffit = pDrip_edge - Methods.angled(c_pitch, thickness);
            double soffit = Math.Max(a_soffit, c_soffit);
            a_unpitched_wall_h = soffit + overhang * Math.Tan(a_pitch);
            c_unpitched_wall_h = soffit + overhang * Math.Tan(c_pitch);
            peak = (bwall * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch) + Math.Max(a_unpitched_wall_h, c_unpitched_wall_h);
            a_pitched_wall = (peak - Math.Max(a_unpitched_wall_h, c_unpitched_wall_h)) / Math.Tan(a_pitch);
            c_pitched_wall = (peak - Math.Max(a_unpitched_wall_h, c_unpitched_wall_h)) / Math.Tan(c_pitch);
            f_peak = peak - (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / Math.Sin(Math.PI - a_pitch - c_pitch);
            max_h = f_peak + Math.Max(Methods.angled(a_pitch, thickness), Methods.angled(c_pitch, thickness)) + (post_width * Math.Sin(a_pitch) * Math.Sin(c_pitch)) / 
                    Math.Sin(Math.PI - a_pitch - c_pitch);
            a_drip_edge = calculate_drip_edge(a_soffit, a_pitch);
            c_drip_edge = calculate_drip_edge(c_soffit, c_pitch);
            calculate_sunroom();
        }
    }
}