using System;
using System.Collections.Generic;

namespace LivingspaceToolkitLib
{
    public class Sunroom
    {
        List <string> endcutList = new List<string>() {"uncut", "plum_T_B", "plum_T"};
        public virtual double overhang {get; private set; }
        public virtual double awall {get; private set; }
        public virtual double bwall {get; private set; }
        public virtual double cwall {get; private set; }
        public virtual double thickness {get; private set; }
        public virtual string endcut {get; private set; }
        public virtual double side_overhang {get; set; }
        public Sunroom(double pOverhang, double pAwall, double pBwall, double pCwall, double pThickness, string pEndcut)
        {
            if (pOverhang >= 0) {overhang = pOverhang;}
            else {throw new System.ArgumentOutOfRangeException("The overhang must be greater than or equal to zero.");}
            if (pAwall >= 0) {awall = pAwall;}
            else {throw new System.ArgumentOutOfRangeException("The awall must be greater than or equal to zero.");}
            if (pBwall >= 0) {bwall = pBwall;}
            else {throw new System.ArgumentOutOfRangeException("The bwall must be greater than or equal to zero.");}
            if (pCwall >= 0) {cwall = pCwall;}
            else {throw new System.ArgumentOutOfRangeException("The cwall must be greater than or equal to zero.");}
            if (pThickness >= 0) {thickness = pThickness;}
            else {throw new System.ArgumentOutOfRangeException("The thickness must be greater than or equal to zero.");}
            if (endcutList.Contains(pEndcut)) {endcut = pEndcut;}
            else {throw new System.Data.DataException($"The listed endcut, {pEndcut}, is not an acceptable input.");}
            if (overhang > 16.0) {side_overhang = 16.0;}
            else {side_overhang = overhang;}
        }
        public virtual double calculate_drip_edge(double soffit, double pitch)
        {
            double angled_thickness = Methods.angled(pitch, thickness);
            if (endcut == "plum_T_B")
            {
                return soffit + angled_thickness;
            }
            else
            {
                return soffit + thickness * Math.Cos(pitch);
            }
        }
        public virtual Dictionary<string, object> calculate_panel_length(double pitch, double pitched_wall)
        {
            Dictionary<string, object> values = new Dictionary<string,object>();
            bool max_panel_length = false;
            bool panel_tolerance = false;
            double p_length, p_bottom, p_top;
            double panel_length;
            if (endcut == "uncut")
            {
                p_length = (pitched_wall + overhang) / Math.Cos(pitch);
            }
            else
            {
                p_bottom = (pitched_wall + overhang) / Math.Cos(pitch);
                p_top = (pitched_wall + overhang + thickness * Math.Sin(pitch)) / Math.Cos(pitch);
                p_length = Math.Max(p_bottom, p_top);
            }
            if (p_length % 12 <= 1) // This checks to see if the panel length is a maximum 1 inch past the nearest foot
            {
                panel_tolerance = true; // If it is 1 inch past the foot then it is within manufaturer's tolerance and is rounded down
                panel_length = Math.Floor(p_length / 12) * 12;
            }
            else
            {
                // If more than one inch past foot then it is rounded up to nearest foot.
                panel_length = Math.Ceiling(p_length / 12) * 12;
            }
            if (panel_length > 288)
            {
                max_panel_length = true;
                panel_length /= 2;
            }
            values.Add("Panel Length", panel_length); // double
            values.Add("Max Length Check", max_panel_length); // bool
            values.Add("Panel Tolerance", panel_tolerance); // bool
            return values;
        }
        protected virtual void calculate_sunroom(){}
    }
}