using System;
using System.Collections.Generic;

namespace LivingspaceToolkitLib
{
    public interface ISunroom
    {
        Dictionary<string, object> calculate_roof_panels(double soffit_wall, Dictionary<string, object> panel_length_dict);
        Dictionary<string, object> calculate_hang_rail(Dictionary<string, object> panel_dict);
        Dictionary<string, object> calculate_fascia(Dictionary<string, object> roof_panel_dict, Dictionary<string, object> panel_length_dict);
    }
}