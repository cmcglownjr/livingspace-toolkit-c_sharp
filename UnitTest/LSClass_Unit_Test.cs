using System;
using System.Collections.Generic;
using Xunit;
using LS = LivingspaceToolkitLib;

namespace LSClass
{
    public class test_Methods
    {

        [Fact]
        public void test_angled()
        {
            double result = LS.Methods.angled(Math.Atan(5/12), 10.25);
            double answer = 10.25 * (Math.Sin(Math.PI / 2) / Math.Sin(Math.PI / 2 - Math.Atan(5/12)));
            Assert.Equal(result, answer);
        }
        [Theory]
        [InlineData("5ft")]
        [InlineData("5feet")]
        [InlineData("5'")]
        [InlineData("5 ft")]
        [InlineData("5 feet")]
        [InlineData("5in")]
        [InlineData("5inch")]
        [InlineData("5\"")]
        [InlineData("5 in")]
        [InlineData("5 inch")]
        public void test_assume_units_given(string input)
        {
            Assert.Equal(LS.Methods.assume_units(input, "ft"), input);
        }
        [Fact]
        public void test_assume_units_assumed()
        {
            string feet = "5ft";
            string inch = "5in";
            Assert.Equal(LS.Methods.assume_units("5", "ft"), feet);
            Assert.Equal(LS.Methods.assume_units("5", "in"), inch);
        }
        [Fact]
        public void test_pitch_input()
        {
            double result, answer1, answer2;
            LS.EngineeringUnits case1 = new LS.EngineeringUnits("24deg", "angle");
            LS.EngineeringUnits case2 = new LS.EngineeringUnits("24", "angle");
            LS.EngineeringUnits case3 = new LS.EngineeringUnits("5in", "length");
            answer1 = Math.PI/180 * 24;
            answer2 = Math.Atan(5.0/12.0);
            result = LS.Methods.pitch_input(case1);
            Assert.Equal(result, answer1);
            result = LS.Methods.pitch_input(case2);
            Assert.Equal(result, answer1);
            result = LS.Methods.pitch_input(case3);
            Assert.Equal(result, answer2);
        }
        [Fact]
        public void test_pitch_estimate()
        {
            double answer1 = 5.0;
            double answer2 = 5.5;
            double answer3 = 6.0;
            Assert.Equal(LS.Methods.pitch_estimate(5.0), answer1);
            Assert.Equal(LS.Methods.pitch_estimate(5.1), answer1);
            Assert.Equal(LS.Methods.pitch_estimate(5.4), answer2);
            Assert.Equal(LS.Methods.pitch_estimate(5.5), answer2);
            Assert.Equal(LS.Methods.pitch_estimate(5.6), answer2);
            Assert.Equal(LS.Methods.pitch_estimate(5.9), answer3);
            Assert.Equal(LS.Methods.pitch_estimate(6.0), answer3);
        }
        [Fact]
        public void test_sixteenth()
        {
        double answer1 = 4.0/16.0;
        double answer2 = 5.0/16.0;
        double answer3 = 6.0/16.0;
        Assert.Equal(LS.Methods.sixteenth(9.0/32.0), answer1);
        Assert.Equal(LS.Methods.sixteenth(10.0/32.0), answer2);
        Assert.Equal(LS.Methods.sixteenth(11.0/32.0), answer3);
        Assert.Equal(LS.Methods.sixteenth(0.37), answer3);
        }
        [Fact]
        public void test_estimate_drip_from_peak()
        {
            double answer1 = 101.125;
            double answer2 = 99.4375;
            double pitch = Math.Atan(5.0/12.0);
            double result1, result2, result3;
            result1 = LS.Methods.sixteenth(LS.Methods.estimate_drip_from_peak(145.0, pitch, 120.0, 12.0, 10.25, 120.0, 144.0, 120.0, "plum_T_B"));
            result2 = LS.Methods.sixteenth(LS.Methods.estimate_drip_from_peak(145.0, pitch, 120.0, 12.0, 10.25, 120.0, 144.0, 120.0, "uncut"));
            result3 = LS.Methods.sixteenth(LS.Methods.estimate_drip_from_peak(145.0, pitch, 120.0, 12.0, 10.25, 120.0, 144.0, 120.0, "plum_T"));
            Assert.Equal(result1, answer1);
            Assert.Equal(result2, answer2);
            Assert.Equal(result3, answer2);
        }
        [Fact]
        public void test_calculate_armstrong_panels()
        {
            double answer = 5;
            double pitch = Math.Atan(5.0/12.0);
            double pitched_wall = 120;
            double unpitched_wall = 144;
            double result = LS.Methods.calculate_armstrong_panels(pitch, pitched_wall, unpitched_wall);
            Assert.Equal(result , answer);
        }
    }
    public class test_Sunroom : IDisposable
    {
        LS.Sunroom sunroom1, sunroom2, sunroom3;
        double pitch = Math.Atan(5.0/12.0);
        public test_Sunroom()
        {
            sunroom1 = new LS.Sunroom(12, 120, 144, 120, 10.25, "plum_T_B");
            sunroom2 = new LS.Sunroom(12, 120, 144, 120, 10.25, "uncut");
            sunroom3 = new LS.Sunroom(12, 281, 144, 281, 10.25, "plum_T_B");
        }
        public void Dispose(){}
        [Fact]
        public void test_calculate_drip_edge()
        {
            double drip1, drip2, result1, result2, answer1, answer2;
            drip1 = sunroom1.calculate_drip_edge(90.0, pitch);
            drip2 = sunroom2.calculate_drip_edge(90.0, pitch);
            result1 = LS.Methods.sixteenth(drip1);
            result2 = LS.Methods.sixteenth(drip2);
            answer1 = 101.125;
            answer2 = 99.4375;
            Assert.Equal(result1, answer1);
            Assert.Equal(result2, answer2);
        }
        [Fact]
        public void test_calculate_panel_length()
        {
            double panel_length_ans1 = 156;
            double panel_length_ans2 = 144;
            Dictionary<string, object> results1 = new Dictionary<string, object>{};
            Dictionary<string, object> results2 = new Dictionary<string, object>{};
            Dictionary<string, object> results3 = new Dictionary<string, object>{};
            results1 = sunroom1.calculate_panel_length(pitch, 120);
            results2 = sunroom2.calculate_panel_length(pitch, 120);
            results3 = sunroom3.calculate_panel_length(Math.Atan(2.4/12.0), 281);
            Assert.Equal(panel_length_ans1, results1["Panel Length"]);
            Assert.False((bool)results1["Max Length Check"]);
            Assert.False((bool)results1["Panel Tolerance"]);
            Assert.Equal(panel_length_ans2, results2["Panel Length"]);
            Assert.False((bool)results2["Max Length Check"]);
            Assert.True((bool)results3["Panel Tolerance"]);
            Assert.True((bool)results3["Max Length Check"]);
        }
        [Fact]
        public void test_sunroom_input()
        {
            Assert.Throws<System.ArgumentOutOfRangeException>(() => new LS.Sunroom(-12, 120, 144, 120, 10.25, "uncut"));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => new LS.Sunroom(12, 120, -144, 120, 10, "uncut"));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => new LS.Sunroom(12, 120, 144, -120, 10, "uncut"));
            Assert.Throws<System.ArgumentOutOfRangeException>(() => new LS.Sunroom(12, 120, 144, 120, -10, "uncut"));
            Assert.Throws<System.Data.DataException>(() => new LS.Sunroom(12, 120, 144, 120, 10, "not_listed"));
        }
    }
    public class test_Studio : IDisposable
    {
        LS.Studio studio1, studio2;
        double pitch = Math.Atan(5.0/12.0);
        public test_Studio()
        {
            studio1 = new LS.Studio(12, 120, 144, 120, 10.25, "plum_T_B");
            studio2 = new LS.Studio(12, 120, 144, 120, 10.25, "uncut");
        }
        public void Dispose(){}
        [Fact]
        public void test_wall_height_pitch()
        {
            double peak, soffit, drip, max, a_c_wall, b_wall;
            studio1.wall_height_pitch(pitch, 95.0);
            studio2.wall_height_pitch(pitch, 95.0);
            peak = 145.0;
            soffit = 90.0;
            drip = 101.125;
            max = 156.125;
            a_c_wall = 95.0;
            b_wall = 95.0;
            Assert.Equal(peak, LS.Methods.sixteenth(studio1.peak));
            Assert.Equal(soffit, LS.Methods.sixteenth(studio1.soffit));
            Assert.Equal(drip, LS.Methods.sixteenth(studio1.drip_edge));
            Assert.Equal(max, LS.Methods.sixteenth(studio1.max_h));
            Assert.Equal(a_c_wall, LS.Methods.sixteenth(studio1.unpitched_wall));
            Assert.Equal(b_wall, LS.Methods.sixteenth(studio1.unpitched_wall));
        }
    }
    public class test_Cathedral : IDisposable
    {
        public LS.Cathedral cathedral1, cathedral2;
        double pitch = Math.Atan(5.0/12.0);
        public test_Cathedral()
        {
            cathedral1 = new LS.Cathedral(12, 120, 144, 120, 10.25, "plum_T_B");
            cathedral2 = new LS.Cathedral(12, 120, 144, 120, 10.25, "uncut");
        }
        public void Dispose(){}
    }
    public class test_Units 
    {
        [Fact]
        public void test_input()
        {
            Assert.Throws<System.ArgumentException>(() => new LS.EngineeringUnits("15N", "force"));
        }
        [Theory]
        [InlineData("24deg")]
        [InlineData("24")]
        public void test_angle(string input)
        {
            double answer = Math.PI/180 * 24;
            LS.EngineeringUnits result = new LS.EngineeringUnits(input, "angle");
            Assert.Equal(result.base_measure, answer);
        }
        [Theory]
        [InlineData("5ft 4 1/2in", 64.5)]
        [InlineData("1/2ft", 6.0)]
        [InlineData("5 1/2ft", 66.0)]
        [InlineData("5 ft", 60.0)]
        [InlineData("1/2in", 0.5)]
        [InlineData("5 1/2in", 5.5)]
        [InlineData("5 in", 5.0)]
        [InlineData("5 3/4 ft 4 5/8 in", 73.625)]
        [InlineData("5\'", 60.0)]
        [InlineData("5\"", 5.0)]
        public void test_length(string input, double expected)
        {
            var EU_test = new LS.EngineeringUnits(input, "length");
            Assert.Equal(EU_test.base_measure, expected);
        }
        [Fact]
        public void test_EU_baseunit()
        {
            string answer = "inch";
            LS.EngineeringUnits result = new LS.EngineeringUnits("5ft 4 1/2in", "length");
            Assert.Equal(result.base_unit, answer);
        }
    }
}
