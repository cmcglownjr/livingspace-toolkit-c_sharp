using System;
using System.Collections.Generic;

namespace LivingspaceToolkit.Models
{
    public class ThicknessCombo
    {
        public List<ThicknessModel> thicknessCreate(Dictionary<string, double> comboList)
        {
            List<ThicknessModel> output = new List<ThicknessModel>();
            foreach (KeyValuePair<string, double> item in comboList)
            {
                output.Add(SetItem(item.Key, item.Value));
            }
            return output;
        }
        private ThicknessModel SetItem(string text, double value)
        {
            ThicknessModel output = new ThicknessModel();
            output.comboText = text;
            output.comboValue = value;
            return output;
        }
    }
}