using System.Collections.Generic;

namespace LivingspaceToolkit.Models
{
    public class ThicknessModel
    {
        public static Dictionary<string, double> eGreen = new Dictionary<string, double>{{"6\"", 6}, {"8\"", 8.25}, {"10\"", 10.25}, {"12\"", 12.25}};
        public static Dictionary<string, double> aluminum = new Dictionary<string, double>{{"3\"", 3}, {"6\"", 6}};
        public static List<string> fasciaList = new List<string>{"3\"", "6\""};
        public string comboText {get; set;}
        public double comboValue {get; set;}
        public override string ToString()
        {
            return comboText;
        }
    }
}