using Avalonia.Controls;
using MessageBox.Avalonia;

namespace Sunroom.Views
{
    public class SunroomViewModel
    {
        static Avalonia.Media.Imaging.Bitmap windowIcon = new Avalonia.Media.Imaging.Bitmap("/Assets/Livingspace_Sunrooms_icon.ico");
        public static void SetPitch(RadioButton ratio, RadioButton angle, TextBox editbox, bool target)
        {
            ratio.IsEnabled = target;
            angle.IsEnabled = target;
            editbox.IsEnabled = target;
            if (!target)
            {
                ratio.IsChecked = target;
                angle.IsChecked = target;
                editbox.Text = "";
            }
            ratio.IsChecked = target;
        }
        public static void SetWall(TextBox awall, TextBox bwall, TextBox cwall, bool target)
        {
            awall.IsEnabled = target;
            bwall.IsEnabled = target;
            cwall.IsEnabled = target;
        }
        public static void SetFascia(CheckBox fascia, bool target)
        {
            fascia.IsEnabled = target;
            fascia.IsChecked = target;
        }
        public static void visibleFascia(TextBlock label1, TextBox results1, TextBlock label2, TextBox results2, bool target)
        {
            label1.IsVisible = target;
            results1.IsVisible = target;
            label2.IsVisible = target;
            results2.IsVisible = target;
        }
        public static void SetEndcuts(RadioButton endcut1, RadioButton endcut2, RadioButton endcut3, bool target)
        {
            endcut1.IsEnabled = target;
            endcut2.IsEnabled = target;
            endcut3.IsEnabled = target;
            if (!target)
            {
                endcut1.IsChecked = target;
                endcut2.IsChecked = target;
                endcut3.IsChecked = target;
            }
        }
        public static void SetRoofing(RadioButton eco, RadioButton al, bool target)
        {
            eco.IsEnabled = target;
            al.IsEnabled = target;
            if (!target)
            {
                eco.IsChecked = target;
                al.IsChecked = target;
            }
        }
        public static string SetEndcutString(RadioButton endcut1, RadioButton endcut2, RadioButton endcut3)
        {
            string endcut = "";
            if ((bool)endcut1.IsChecked){endcut = "uncut";}
            else if ((bool)endcut2.IsChecked){endcut = "plum_T_B";}
            else if ((bool)endcut3.IsChecked){endcut = "plum_T";}
            return endcut;
        }
        public static void SunroomMessageBox(string title, string message)
        {
            var msBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager
            .GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams{
                ContentTitle = title,
                ContentMessage = message,
                Icon = MessageBox.Avalonia.Enums.Icon.Error,
                // WindowIcon = windowIcon,
                WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterScreen
            });
            msBoxStandardWindow.Show();
        }
        
    }
}