using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using System.Reactive;
using System.Reactive.Linq;
using System.Collections.Generic;
using LivingspaceToolkit.Models;
using LivingspaceToolkit.ViewModels;
using LivingspaceToolkitLib;
using System;

namespace Sunroom.Views
{
    public class StudioView : UserControl
    {
        public StudioView()
        {
            InitializeComponent();
            DataContext = new StudioViewModel(); 
            st_scenario1 = this.Find<RadioButton>("st_scenario1");
            st_scenario2 = this.Find<RadioButton>("st_scenario2");
            st_scenario3 = this.Find<RadioButton>("st_scenario3");
            st_scenario4 = this.Find<RadioButton>("st_scenario4");
            st_scenario5 = this.Find<RadioButton>("st_scenario5");
            st_scenario6 = this.Find<RadioButton>("st_scenario6");
            st_scenario7 = this.Find<RadioButton>("st_scenario7");
            st_ratio_radio = this.Find<RadioButton>("st_ratio_radio");
            st_angle_radio = this.Find<RadioButton>("st_angle_radio");
            st_pitch_editbox = this.Find<TextBox>("st_pitch_editbox");
            st_overhang_editbox = this.Find<TextBox>("st_overhang_editbox");
            st_eco_radio = this.Find<RadioButton>("st_eco_radio");
            st_al_radio = this.Find<RadioButton>("st_al_radio");
            st_thick_combo = this.Find<ComboBox>("st_thick_combo");
            st_endcut1 = this.Find<RadioButton>("st_endcut1");
            st_endcut2 = this.Find<RadioButton>("st_endcut2");
            st_endcut3 = this.Find<RadioButton>("st_endcut3");
            st_fascia = this.Find<CheckBox>("st_fascia");
            st_peak_editbox = this.Find<TextBox>("st_peak_editbox");
            st_max_editbox = this.Find<TextBox>("st_max_editbox");
            st_bwallheight_editbox = this.Find<TextBox>("st_bwallheight_editbox");
            st_soffit_editbox = this.Find<TextBox>("st_soffit_editbox");
            st_drip_editbox = this.Find<TextBox>("st_drip_editbox");
            st_awall_editbox = this.Find<TextBox>("st_awall_editbox");
            st_bwall_editbox = this.Find<TextBox>("st_bwall_editbox");
            st_cwall_editbox = this.Find<TextBox>("st_cwall_editbox");
            btn_st_calculate = this.Find<Button>("btn_st_calculate");
            st_PitchResult = this.Find<TextBox>("st_PitchResult");
            st_PeakResult = this.Find<TextBox>("st_PeakResult");
            st_SoffitResult = this.Find<TextBox>("st_SoffitResult");
            st_DripResult = this.Find<TextBox>("st_DripResult");
            st_MaxResult = this.Find<TextBox>("st_MaxResult");
            st_WallHeightResult = this.Find<TextBox>("st_WallHeightResult");
            st_RoofPanelsResult = this.Find<TextBox>("st_RoofPanelsResult");
            st_PanelLengthResult = this.Find<TextBox>("st_PanelLengthResult");
            st_RoofSqFtResult = this.Find<TextBox>("st_RoofSqFtResult");
            st_CeilingPanelsResult = this.Find<TextBox>("st_CeilingPanelsResult");
            st_OverhangResult = this.Find<TextBox>("st_OverhangResult");
            st_OverhangResult2 = this.Find<TextBox>("st_OverhangResult2");
            st_HangRailsResult = this.Find<TextBox>("st_HangRailsResult");
            st_FasciaResult = this.Find<TextBox>("st_FasciaResult");
            st_FasciaResult2 = this.Find<TextBox>("st_FasciaResult2");
            st_FasciaResult_label = this.Find<TextBlock>("st_FasciaResult_label");
            st_FasciaResult_label2 = this.Find<TextBlock>("st_FasciaResult_label2");
            DisableAll();
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
        // Scenarios
        public static RadioButton st_scenario1, st_scenario2, st_scenario3, st_scenario4, st_scenario5, st_scenario6, st_scenario7;
        // Controls
        public static RadioButton st_ratio_radio, st_angle_radio, st_eco_radio, st_al_radio, st_endcut1, st_endcut2, st_endcut3;
        public static TextBox st_pitch_editbox, st_overhang_editbox, st_peak_editbox, st_max_editbox, st_bwallheight_editbox;
        public static TextBox st_soffit_editbox, st_drip_editbox, st_awall_editbox, st_bwall_editbox, st_cwall_editbox;
        // Results
        public static TextBox st_FasciaResult, st_FasciaResult2, st_PitchResult, st_PeakResult, st_SoffitResult, st_DripResult, st_MaxResult;
        public static TextBox st_WallHeightResult, st_RoofPanelsResult, st_PanelLengthResult, st_RoofSqFtResult, st_CeilingPanelsResult;
        public static TextBox st_OverhangResult, st_OverhangResult2, st_HangRailsResult;
        public static ComboBox st_thick_combo;
        public static CheckBox st_fascia;
        public static Button btn_st_calculate;
        public static TextBlock st_FasciaResult_label, st_FasciaResult_label2;
        public static void DisableAll()
        {
            SunroomViewModel.SetPitch(st_ratio_radio, st_angle_radio, st_pitch_editbox, false);
            st_overhang_editbox.IsEnabled = false;
            SunroomViewModel.SetRoofing(st_eco_radio, st_al_radio, false);
            st_thick_combo.IsEnabled = true;
            st_peak_editbox.IsEnabled = false;
            st_max_editbox.IsEnabled = false;
            st_bwallheight_editbox.IsEnabled = false;
            st_soffit_editbox.IsEnabled = false;
            st_drip_editbox.IsEnabled = false;
            SunroomViewModel.SetEndcuts(st_endcut1, st_endcut2, st_endcut3, false);
            SunroomViewModel.SetWall(st_awall_editbox, st_bwall_editbox, st_cwall_editbox, false);
            SunroomViewModel.SetFascia(st_fascia, false);
            SunroomViewModel.visibleFascia(st_FasciaResult_label, st_FasciaResult, st_FasciaResult_label2, st_FasciaResult2, false);
        }
        public static void Scenarios()
        {
            DisableAll();
            st_overhang_editbox.IsEnabled = true;
            SunroomViewModel.SetRoofing(st_eco_radio, st_al_radio, true);
            st_thick_combo.IsEnabled = true;
            st_awall_editbox.IsEnabled = true;
            st_bwall_editbox.IsEnabled = true;
            st_cwall_editbox.IsEnabled = true;
            if ((bool)st_scenario1.IsChecked)
            {
                SunroomViewModel.SetPitch(st_ratio_radio, st_angle_radio, st_pitch_editbox, true);
                st_bwallheight_editbox.IsEnabled = true;
            }
            else if ((bool)st_scenario2.IsChecked)
            {
                st_peak_editbox.IsEnabled = true;
                st_bwallheight_editbox.IsEnabled = true;
            }
            else if ((bool)st_scenario3.IsChecked)
            {
                SunroomViewModel.SetPitch(st_ratio_radio, st_angle_radio, st_pitch_editbox, true);
                st_max_editbox.IsEnabled = true;
            }
            else if ((bool)st_scenario4.IsChecked)
            {
                st_soffit_editbox.IsEnabled = true;
                st_peak_editbox.IsEnabled = true;
            }
            else if ((bool)st_scenario5.IsChecked)
            {
                SunroomViewModel.SetPitch(st_ratio_radio, st_angle_radio, st_pitch_editbox, true);
                st_soffit_editbox.IsEnabled = true;
            }
            else if ((bool)st_scenario6.IsChecked)
            {
                st_drip_editbox.IsEnabled = true;
                st_peak_editbox.IsEnabled = true;
            }
            else if ((bool)st_scenario7.IsChecked)
            {
                SunroomViewModel.SetPitch(st_ratio_radio, st_angle_radio, st_pitch_editbox, true);
                st_drip_editbox.IsEnabled = true;
            }
            else{DisableAll();}
        }
        public static void Endcuts()
        {
            SunroomViewModel.SetEndcuts(st_endcut1, st_endcut2, st_endcut3, false);
            st_endcut1.IsChecked = false;
            st_endcut2.IsChecked = false;
            st_endcut3.IsChecked = false;
            if ((bool)st_eco_radio.IsChecked)
            {
                SunroomViewModel.SetEndcuts(st_endcut1, st_endcut2, st_endcut3, true);
                st_endcut1.IsChecked = true;
                SunroomViewModel.SetFascia(st_fascia, true);
                SunroomViewModel.visibleFascia(st_FasciaResult_label, st_FasciaResult, st_FasciaResult_label2, st_FasciaResult2, true);
            }
            else if ((bool)st_al_radio.IsChecked)
            {
                st_endcut1.IsEnabled = true;
                st_endcut1.IsChecked = true;
                SunroomViewModel.SetFascia(st_fascia, true);
                SunroomViewModel.visibleFascia(st_FasciaResult_label, st_FasciaResult, st_FasciaResult_label2, st_FasciaResult2, true);
            }
            st_thick_combo.SelectedIndex = 0;
        }
        public static void EndcutFascia()
        {
            if ((bool)st_endcut2.IsChecked)
            {
                SunroomViewModel.SetFascia(st_fascia, false);
                SunroomViewModel.visibleFascia(st_FasciaResult_label, st_FasciaResult, st_FasciaResult_label2, st_FasciaResult2, false);
            }
            else 
            {
                SunroomViewModel.SetFascia(st_fascia, fasciaThicknessCheck());
                SunroomViewModel.visibleFascia(st_FasciaResult_label, st_FasciaResult, st_FasciaResult_label2, st_FasciaResult2, fasciaThicknessCheck());
            }
        }
        public void stThicknessSelectionChange(object sender, SelectionChangedEventArgs e)
        {
            SunroomViewModel.SetFascia(st_fascia, fasciaThicknessCheck());
            SunroomViewModel.visibleFascia(st_FasciaResult_label, st_FasciaResult, st_FasciaResult_label2, st_FasciaResult2, fasciaThicknessCheck());
            st_endcut1.IsChecked = true;
        }
        public static bool fasciaThicknessCheck()
        {
            string selected = st_thick_combo.SelectedItem.ToString();
            if (ThicknessModel.fasciaList.Contains(selected) & (!(bool)st_endcut2.IsChecked))
            {return true;}
            else {return false;}
        }
        public void stFasciaCheckboxChange(object sender, RoutedEventArgs e)
        {
            bool checkboxStat = (bool)st_fascia.IsChecked;
            SunroomViewModel.visibleFascia(st_FasciaResult_label, st_FasciaResult, st_FasciaResult_label2, st_FasciaResult2, checkboxStat);
        }
        public static void stButton()
        {
            List<bool> initialConditions = new List<bool>{false, false, false, false};
            ThicknessModel stComboItem = new ThicknessModel();
            EngineeringUnits overhang = default(EngineeringUnits);
            EngineeringUnits aWall = default(EngineeringUnits);
            EngineeringUnits bWall = default(EngineeringUnits);
            EngineeringUnits cWall = default(EngineeringUnits);
            EngineeringUnits pitch = default(EngineeringUnits);
            EngineeringUnits peakHeight = default(EngineeringUnits);
            EngineeringUnits maxHeight = default(EngineeringUnits);
            EngineeringUnits bWallHeight = default(EngineeringUnits);
            EngineeringUnits soffitHeight = default(EngineeringUnits);
            EngineeringUnits dripEdgeHeight = default(EngineeringUnits);
            string endcut = SunroomViewModel.SetEndcutString(st_endcut1, st_endcut2, st_endcut3);
            double roofArea = default(double);
            // Overhang
            try 
            {
                overhang = new EngineeringUnits(Methods.assume_units(st_overhang_editbox.Text, "\""), "length"); 
                initialConditions[0] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("Overhang Error", e.Message);}
            // A wall
            try 
            {
                aWall = new EngineeringUnits(Methods.assume_units(st_awall_editbox.Text, "\""), "length");
                initialConditions[1] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("A Wall Error", e.Message);}
            // B Wall
            try 
            {
                bWall = new EngineeringUnits(Methods.assume_units(st_bwall_editbox.Text, "\""), "length"); 
                initialConditions[2] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("B Wall Error", e.Message);}
            // C Wall
            try 
            {
                cWall = new EngineeringUnits(Methods.assume_units(st_cwall_editbox.Text, "\""), "length");
                initialConditions[3] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("C Wall Error", e.Message);}
            // Thickness combobox item
            stComboItem = (ThicknessModel)st_thick_combo.SelectedItem;
            // Create Sunroom Object
            if (initialConditions[0] && initialConditions[1] && initialConditions[2] && initialConditions[3])
            {
                Studio studio = new Studio(overhang.base_measure, aWall.base_measure, bWall.base_measure, cWall.base_measure, stComboItem.comboValue, endcut);
                if ((bool)st_scenario1.IsChecked) // Pitch and B Wall Height
                {
                    try
                    {
                        if ((bool)st_ratio_radio.IsChecked) 
                        {pitch = new EngineeringUnits(Methods.assume_units(st_pitch_editbox.Text, "\""), "length");}
                        if ((bool)st_angle_radio.IsChecked) 
                        {pitch = new EngineeringUnits(st_pitch_editbox.Text, "angle");}
                        try
                        {
                            bWallHeight = new EngineeringUnits(Methods.assume_units(st_bwallheight_editbox.Text, "\""), "length");
                            studio.wall_height_pitch(Methods.pitch_input(pitch), bWallHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("B Wall Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                if ((bool)st_scenario2.IsChecked) // B Wall and Peak Height
                {
                    try
                    {
                        
                        bWallHeight = new EngineeringUnits(Methods.assume_units(st_bwallheight_editbox.Text, "\""), "length");
                        try
                        {
                            peakHeight = new EngineeringUnits(Methods.assume_units(st_peak_editbox.Text, "\""), "length");
                            studio.wall_height_peak_height(bWallHeight.base_measure, peakHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Peak Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("B Wall Height Error", e.Message);}
                }
                if ((bool)st_scenario3.IsChecked) // Max Height and Pitch
                {
                    try
                    {
                        
                        if ((bool)st_ratio_radio.IsChecked) 
                        {pitch = new EngineeringUnits(Methods.assume_units(st_pitch_editbox.Text, "\""), "length");}
                        if ((bool)st_angle_radio.IsChecked) 
                        {pitch = new EngineeringUnits(st_pitch_editbox.Text, "angle");}
                        try
                        {
                            maxHeight = new EngineeringUnits(Methods.assume_units(st_max_editbox.Text, "\""), "length");
                            studio.max_height_pitch(Methods.pitch_input(pitch), maxHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Max Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                if ((bool)st_scenario4.IsChecked) // Soffit and Peak Height
                {
                    try
                    {
                        soffitHeight = new EngineeringUnits(Methods.assume_units(st_soffit_editbox.Text, "\""), "length");
                        try
                        {
                            peakHeight = new EngineeringUnits(Methods.assume_units(st_peak_editbox.Text, "\""), "length");
                            studio.soffit_height_peak_height(soffitHeight.base_measure, peakHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Peak Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Soffit Height Error", e.Message);}
                }
                if ((bool)st_scenario5.IsChecked) // Soffit Height and Pitch
                {
                    try
                    {
                        
                        if ((bool)st_ratio_radio.IsChecked) 
                        {pitch = new EngineeringUnits(Methods.assume_units(st_pitch_editbox.Text, "\""), "length");}
                        if ((bool)st_angle_radio.IsChecked) 
                        {pitch = new EngineeringUnits(st_pitch_editbox.Text, "angle");}
                        try
                        {
                            soffitHeight = new EngineeringUnits(Methods.assume_units(st_soffit_editbox.Text, "\""), "length");
                            studio.soffit_height_pitch(Methods.pitch_input(pitch), soffitHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Soffit Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                if ((bool)st_scenario6.IsChecked) // Drip Edge and Peak Height
                {
                    try
                    {
                        
                        dripEdgeHeight = new EngineeringUnits(Methods.assume_units(st_drip_editbox.Text, "\""), "length");
                        try
                        {
                            peakHeight = new EngineeringUnits(Methods.assume_units(st_peak_editbox.Text, "\""), "length");
                            studio.drip_edge_peak_height(dripEdgeHeight.base_measure, peakHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Peak Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Drip Edge Error", e.Message);}
                }
                if ((bool)st_scenario7.IsChecked) // Drip Edge and Pitch
                {
                    try
                    {
                        
                        if ((bool)st_ratio_radio.IsChecked) 
                        {pitch = new EngineeringUnits(Methods.assume_units(st_pitch_editbox.Text, "\""), "length");}
                        if ((bool)st_angle_radio.IsChecked) 
                        {pitch = new EngineeringUnits(st_pitch_editbox.Text, "angle");}
                        try
                        {
                            dripEdgeHeight = new EngineeringUnits(Methods.assume_units(st_drip_editbox.Text, "\""), "length");
                            studio.drip_edge_pitch(dripEdgeHeight.base_measure, Methods.pitch_input(pitch));
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Drip Edge Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                roofArea = (double)studio.roof_panel_dict["Roof Area"] / 144.0;
                List<bool> fasciaCheck = (List<bool>)studio.fascia_dict["Fascia Check"];
                st_PitchResult.Text = Methods.pitch_estimate(12 * Math.Tan(studio.pitch)).ToString() + "/12";
                st_PeakResult.Text = Methods.sixteenth(studio.peak).ToString() + " in.";
                st_SoffitResult.Text = Methods.sixteenth(studio.soffit).ToString() + " in.";
                st_DripResult.Text = Methods.sixteenth(studio.drip_edge).ToString() + " in.";
                st_MaxResult.Text = Methods.sixteenth(studio.max_h).ToString() + " in.";
                st_WallHeightResult.Text = Methods.sixteenth(studio.unpitched_wall).ToString() + " in.";
                st_RoofPanelsResult.Text = studio.roof_panel_dict["Roof Panels"].ToString();
                st_PanelLengthResult.Text = studio.panel_length_dict["Panel Length"].ToString() + " in.";
                st_RoofSqFtResult.Text = Math.Round(roofArea, 0).ToString() + " sq.ft.";
                st_CeilingPanelsResult.Text = studio.armstrong_panels.ToString();
                st_OverhangResult.Text = Methods.sixteenth(studio.overhang).ToString() + " in.";
                st_OverhangResult2.Text = studio.roof_panel_dict["Side Overhang"].ToString() + " in.";
                if ((bool)studio.hang_rail_dict["Hang Rail Check"])
                {
                    st_HangRailsResult.Text = "There are 2 pairs of\nhang rails at " + studio.hang_rail_dict["Hang Rail"].ToString() + " in.";
                }
                else
                {
                    st_HangRailsResult.Text = "There is 1 pair of \nhang rails at " + studio.hang_rail_dict["Hang Rail"].ToString() + " in.";
                }
                if (fasciaCheck[0])
                {
                    st_FasciaResult.Text = "There are 2 pieces of\nfascia at " + studio.fascia_dict["Wall Fascia"].ToString() + " in. for the\nB Wall.";
                }
                else
                {
                    st_FasciaResult.Text = "There is 1 piece of\nfascia at " + studio.fascia_dict["Wall Fascia"].ToString() + " in. for the\nB Wall.";
                }
                if (fasciaCheck[1])
                {
                    st_FasciaResult2.Text = "There are 2 pieces of\nfascia at " + studio.fascia_dict["Side Fascia"].ToString() + " in. for the\nA & C Walls.";
                }
                else
                {
                    st_FasciaResult2.Text = "There is 1 piece of\nfascia at " + studio.fascia_dict["Side Fascia"].ToString() + " in. for the\nA & C Walls.";
                }
            }
            else {SunroomViewModel.SunroomMessageBox("Calculation Error", "Missing Inputs");}
        }
    }
    
}