using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using ReactiveUI;
using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Collections.Generic;
using LivingspaceToolkit.Models;
using LivingspaceToolkit.ViewModels;
using LivingspaceToolkitLib;

namespace Sunroom.Views
{
    public class CathedralView : UserControl
    {
        public CathedralView()
        {
            InitializeComponent();
            DataContext = new CathedralViewModel(); 
            ca_scenario1 = this.Find<RadioButton>("ca_scenario1");
            ca_scenario2 = this.Find<RadioButton>("ca_scenario2");
            ca_scenario3 = this.Find<RadioButton>("ca_scenario3");
            ca_scenario4 = this.Find<RadioButton>("ca_scenario4");
            ca_scenario5 = this.Find<RadioButton>("ca_scenario5");
            ca_scenario6 = this.Find<RadioButton>("ca_scenario6");
            ca_scenario7 = this.Find<RadioButton>("ca_scenario7");
            ca_a_ratio_radio = this.Find<RadioButton>("ca_a_ratio_radio");
            ca_a_angle_radio = this.Find<RadioButton>("ca_a_angle_radio");
            ca_a_pitch_editbox = this.Find<TextBox>("ca_a_pitch_editbox");
            ca_c_ratio_radio = this.Find<RadioButton>("ca_c_ratio_radio");
            ca_c_angle_radio = this.Find<RadioButton>("ca_c_angle_radio");
            ca_c_pitch_editbox = this.Find<TextBox>("ca_c_pitch_editbox");
            ca_overhang_editbox = this.Find<TextBox>("ca_overhang_editbox");
            ca_eco_radio = this.Find<RadioButton>("ca_eco_radio");
            ca_al_radio = this.Find<RadioButton>("ca_al_radio");
            ca_thick_combo = this.Find<ComboBox>("ca_thick_combo");
            ca_endcut1 = this.Find<RadioButton>("ca_endcut1");
            ca_endcut2 = this.Find<RadioButton>("ca_endcut2");
            ca_endcut3 = this.Find<RadioButton>("ca_endcut3");
            ca_fascia = this.Find<CheckBox>("ca_fascia");
            ca_peak_editbox = this.Find<TextBox>("ca_peak_editbox");
            ca_max_editbox = this.Find<TextBox>("ca_max_editbox");
            ca_awallheight_editbox = this.Find<TextBox>("ca_awallheight_editbox");
            ca_cwallheight_editbox = this.Find<TextBox>("ca_cwallheight_editbox");
            ca_asoffit_editbox = this.Find<TextBox>("ca_asoffit_editbox");
            ca_csoffit_editbox = this.Find<TextBox>("ca_csoffit_editbox");
            ca_drip_editbox = this.Find<TextBox>("ca_drip_editbox");
            ca_awall_editbox = this.Find<TextBox>("ca_awall_editbox");
            ca_bwall_editbox = this.Find<TextBox>("ca_bwall_editbox");
            ca_cwall_editbox = this.Find<TextBox>("ca_cwall_editbox");
            btn_ca_calculate = this.Find<Button>("btn_ca_calculate");
            ca_aSidePitchResult = this.Find<TextBox>("ca_aSidePitchResult");
            ca_cSidePitchResult = this.Find<TextBox>("ca_cSidePitchResult");
            ca_peakHeightResult = this.Find<TextBox>("ca_peakHeightResult");
            ca_aSoffitHeightResult = this.Find<TextBox>("ca_aSoffitHeightResult");
            ca_cSoffitHeightResult = this.Find<TextBox>("ca_cSoffitHeightResult");
            ca_aDripResult = this.Find<TextBox>("ca_aDripResult");
            ca_cDripResult = this.Find<TextBox>("ca_cDripResult");
            ca_maxResult = this.Find<TextBox>("ca_maxResult");
            ca_aWallResult = this.Find<TextBox>("ca_aWallResult");
            ca_cWallResult = this.Find<TextBox>("ca_cWallResult");
            ca_aPanelsResult = this.Find<TextBox>("ca_aPanelsResult");
            ca_aPanelLengthResult = this.Find<TextBox>("ca_aPanelLengthResult");
            ca_cPanelsResult = this.Find<TextBox>("ca_cPanelsResult");
            ca_cPanelLengthResult = this.Find<TextBox>("ca_cPanelLengthResult");
            ca_totalRoofResult = this.Find<TextBox>("ca_totalRoofResult");
            ca_totalPanelsResult = this.Find<TextBox>("ca_totalPanelsResult");
            ca_ceilingPanelsResult = this.Find<TextBox>("ca_ceilingPanelsResult");
            ca_aOverhangResult = this.Find<TextBox>("ca_aOverhangResult");
            ca_bOverhangResult = this.Find<TextBox>("ca_bOverhangResult");
            ca_cOverhangResult = this.Find<TextBox>("ca_cOverhangResult");
            ca_aHangRailsResult = this.Find<TextBox>("ca_aHangRailsResult");
            ca_cHangRailsResult = this.Find<TextBox>("ca_cHangRailsResult");
            ca_aFasciaResult = this.Find<TextBox>("ca_aFasciaResult");
            ca_cFasciaResult = this.Find<TextBox>("ca_cFasciaResult");
            ca_aFasciaResult2 = this.Find<TextBox>("ca_aFasciaResult2");
            ca_cFasciaResult2 = this.Find<TextBox>("ca_cFasciaResult2");
            ca_aFasciaResultLabel = this.Find<TextBlock>("ca_aFasciaResultLabel");
            ca_cFasciaResultLabel = this.Find<TextBlock>("ca_cFasciaResultLabel");
            ca_aFasciaResultLabel2 = this.Find<TextBlock>("ca_aFasciaResultLabel2");
            ca_cFasciaResultLabel2 = this.Find<TextBlock>("ca_cFasciaResultLabel2");
            DisableAll();
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
        // Scenarios
        public static RadioButton ca_scenario1, ca_scenario2, ca_scenario3, ca_scenario4, ca_scenario5, ca_scenario6, ca_scenario7;
        // Controls
        public static RadioButton ca_a_ratio_radio, ca_a_angle_radio, ca_c_ratio_radio, ca_c_angle_radio, ca_eco_radio, ca_al_radio;
        public static RadioButton ca_endcut1, ca_endcut2, ca_endcut3;
        public static TextBox ca_a_pitch_editbox, ca_c_pitch_editbox, ca_overhang_editbox, ca_peak_editbox, ca_max_editbox;
        public static TextBox ca_awallheight_editbox, ca_cwallheight_editbox, ca_asoffit_editbox, ca_csoffit_editbox; 
        public static TextBox ca_drip_editbox, ca_awall_editbox, ca_bwall_editbox, ca_cwall_editbox;
        public static ComboBox ca_thick_combo;
        public static CheckBox ca_fascia;
        public static Button btn_ca_calculate;
        // Results section
        public static TextBox ca_aSidePitchResult, ca_cSidePitchResult, ca_peakHeightResult, ca_aSoffitHeightResult, ca_cSoffitHeightResult, ca_ceilingPanelsResult;
        public static TextBox ca_aDripResult, ca_cDripResult, ca_maxResult, ca_aWallResult, ca_cWallResult, ca_aPanelsResult, ca_aPanelLengthResult;
        public static TextBox ca_cPanelsResult, ca_cPanelLengthResult, ca_totalRoofResult, ca_totalPanelsResult, ca_aOverhangResult, ca_bOverhangResult;
        public static TextBox ca_cOverhangResult, ca_aHangRailsResult, ca_cHangRailsResult, ca_aFasciaResult, ca_cFasciaResult, ca_aFasciaResult2, ca_cFasciaResult2;
        public static TextBlock ca_aFasciaResultLabel, ca_cFasciaResultLabel, ca_aFasciaResultLabel2, ca_cFasciaResultLabel2;
        public static void DisableAll()
        {
            SunroomViewModel.SetPitch(ca_a_ratio_radio, ca_a_angle_radio, ca_a_pitch_editbox, false);
            SunroomViewModel.SetPitch(ca_c_ratio_radio, ca_c_angle_radio, ca_c_pitch_editbox, false);
            ca_overhang_editbox.IsEnabled = false;
            SunroomViewModel.SetRoofing(ca_eco_radio, ca_al_radio, false);
            ca_thick_combo.IsEnabled = true;
            ca_peak_editbox.IsEnabled = false;
            ca_max_editbox.IsEnabled = false;
            ca_awallheight_editbox.IsEnabled = false;
            ca_cwallheight_editbox.IsEnabled = false;
            ca_asoffit_editbox.IsEnabled = false;
            ca_csoffit_editbox.IsEnabled = false;
            ca_drip_editbox.IsEnabled = false;
            SunroomViewModel.SetEndcuts(ca_endcut1, ca_endcut2, ca_endcut3, false);
            SunroomViewModel.SetWall(ca_awall_editbox, ca_bwall_editbox, ca_cwall_editbox, false);
            SunroomViewModel.SetFascia(ca_fascia, false);
            SunroomViewModel.visibleFascia(ca_aFasciaResultLabel, ca_aFasciaResult, ca_aFasciaResultLabel2, ca_aFasciaResult2, false);
            SunroomViewModel.visibleFascia(ca_cFasciaResultLabel, ca_cFasciaResult, ca_cFasciaResultLabel2, ca_cFasciaResult2, false);
        }
        public static void Scenarios()
        {
            DisableAll();
            ca_overhang_editbox.IsEnabled = true;
            SunroomViewModel.SetRoofing(ca_eco_radio, ca_al_radio, true);
            ca_thick_combo.IsEnabled = true;
            ca_awall_editbox.IsEnabled = true;
            ca_bwall_editbox.IsEnabled = true;
            ca_cwall_editbox.IsEnabled = true;
            if ((bool)ca_scenario1.IsChecked)
            {
                SunroomViewModel.SetPitch(ca_a_ratio_radio, ca_a_angle_radio, ca_a_pitch_editbox, true);
                SunroomViewModel.SetPitch(ca_c_ratio_radio, ca_c_angle_radio, ca_c_pitch_editbox, true);
                ca_awallheight_editbox.IsEnabled = true;
                ca_cwallheight_editbox.IsEnabled = true;
            }
            else if ((bool)ca_scenario2.IsChecked)
            {
                ca_peak_editbox.IsEnabled = true;
                ca_awallheight_editbox.IsEnabled = true;
                ca_cwallheight_editbox.IsEnabled = true;
            }
            else if ((bool)ca_scenario3.IsChecked)
            {
                SunroomViewModel.SetPitch(ca_a_ratio_radio, ca_a_angle_radio, ca_a_pitch_editbox, true);
                SunroomViewModel.SetPitch(ca_c_ratio_radio, ca_c_angle_radio, ca_c_pitch_editbox, true);
                ca_max_editbox.IsEnabled = true;
            }
            else if ((bool)ca_scenario4.IsChecked)
            {
                ca_asoffit_editbox.IsEnabled = true;
                ca_csoffit_editbox.IsEnabled = true;
                ca_peak_editbox.IsEnabled = true;
            }
            else if ((bool)ca_scenario5.IsChecked)
            {
                SunroomViewModel.SetPitch(ca_a_ratio_radio, ca_a_angle_radio, ca_a_pitch_editbox, true);
                SunroomViewModel.SetPitch(ca_c_ratio_radio, ca_c_angle_radio, ca_c_pitch_editbox, true);
                ca_asoffit_editbox.IsEnabled = true;
                ca_csoffit_editbox.IsEnabled = true;
            }
            else if ((bool)ca_scenario6.IsChecked)
            {
                ca_drip_editbox.IsEnabled = true;
                ca_peak_editbox.IsEnabled = true;
            }
            else if ((bool)ca_scenario7.IsChecked)
            {
                SunroomViewModel.SetPitch(ca_a_ratio_radio, ca_a_angle_radio, ca_a_pitch_editbox, true);
                SunroomViewModel.SetPitch(ca_c_ratio_radio, ca_c_angle_radio, ca_c_pitch_editbox, true);
                ca_drip_editbox.IsEnabled = true;
            }
            else{DisableAll();}
        }
        public static void Endcuts()
        {
            SunroomViewModel.SetEndcuts(ca_endcut1, ca_endcut2, ca_endcut3, false);
            ca_endcut1.IsChecked = false;
            ca_endcut2.IsChecked = false;
            ca_endcut3.IsChecked = false;
            if ((bool)ca_eco_radio.IsChecked)
            {
                SunroomViewModel.SetEndcuts(ca_endcut1, ca_endcut2, ca_endcut3, true);
                ca_endcut1.IsChecked = true;
                SunroomViewModel.SetFascia(ca_fascia, true);
                SunroomViewModel.visibleFascia(ca_aFasciaResultLabel, ca_aFasciaResult, ca_aFasciaResultLabel2, ca_aFasciaResult2, true);
                SunroomViewModel.visibleFascia(ca_cFasciaResultLabel, ca_cFasciaResult, ca_cFasciaResultLabel2, ca_cFasciaResult2, true);
            }
            else if ((bool)ca_al_radio.IsChecked)
            {
                ca_endcut1.IsEnabled = true;
                ca_endcut1.IsChecked = true;
                SunroomViewModel.SetFascia(ca_fascia, true);
                SunroomViewModel.visibleFascia(ca_aFasciaResultLabel, ca_aFasciaResult, ca_aFasciaResultLabel2, ca_aFasciaResult2, true);
                SunroomViewModel.visibleFascia(ca_cFasciaResultLabel, ca_cFasciaResult, ca_cFasciaResultLabel2, ca_cFasciaResult2, true);
            }
            ca_thick_combo.SelectedIndex = 0;
        }
        public static void EndcutFascia()
        {
            if ((bool)ca_endcut2.IsChecked)
            {
                SunroomViewModel.SetFascia(ca_fascia, false);
                SunroomViewModel.visibleFascia(ca_aFasciaResultLabel, ca_aFasciaResult, ca_aFasciaResultLabel2, ca_aFasciaResult2, false);
                SunroomViewModel.visibleFascia(ca_cFasciaResultLabel, ca_cFasciaResult, ca_cFasciaResultLabel2, ca_cFasciaResult2, false);
            }
            else 
            {
                SunroomViewModel.SetFascia(ca_fascia, fasciaThicknessCheck());
                SunroomViewModel.visibleFascia(ca_aFasciaResultLabel, ca_aFasciaResult, ca_aFasciaResultLabel2, ca_aFasciaResult2, fasciaThicknessCheck());
                SunroomViewModel.visibleFascia(ca_cFasciaResultLabel, ca_cFasciaResult, ca_cFasciaResultLabel2, ca_cFasciaResult2, fasciaThicknessCheck());
            }
        }
        public void caThicknessSelectionChange(object sender, SelectionChangedEventArgs e)
        {
            SunroomViewModel.SetFascia(ca_fascia, fasciaThicknessCheck());
            SunroomViewModel.visibleFascia(ca_aFasciaResultLabel, ca_aFasciaResult, ca_aFasciaResultLabel2, ca_aFasciaResult2, fasciaThicknessCheck());
            SunroomViewModel.visibleFascia(ca_cFasciaResultLabel, ca_cFasciaResult, ca_cFasciaResultLabel2, ca_cFasciaResult2, fasciaThicknessCheck());
            ca_endcut1.IsChecked = true;
        }
        public void caFasciaCheckboxChange(object sender, RoutedEventArgs e)
        {
            bool checkboxStat = (bool)ca_fascia.IsChecked;
            SunroomViewModel.visibleFascia(ca_aFasciaResultLabel, ca_aFasciaResult, ca_aFasciaResultLabel2, ca_aFasciaResult2, checkboxStat);
            SunroomViewModel.visibleFascia(ca_cFasciaResultLabel, ca_cFasciaResult, ca_cFasciaResultLabel2, ca_cFasciaResult2, checkboxStat);
        }
        public static bool fasciaThicknessCheck()
        {
            string selected = ca_thick_combo.SelectedItem.ToString();
            if (ThicknessModel.fasciaList.Contains(selected) & (!(bool)ca_endcut2.IsChecked))
            {return true;}
            else {return false;}
        }
        public static void caButton()
        {
            List<bool> initialConditions = new List<bool>{false, false, false, false};
            ThicknessModel ComboItem = new ThicknessModel();
            EngineeringUnits overhang = default(EngineeringUnits);
            EngineeringUnits aWall = default(EngineeringUnits);
            EngineeringUnits bWall = default(EngineeringUnits);
            EngineeringUnits cWall = default(EngineeringUnits);
            EngineeringUnits aPitch = default(EngineeringUnits);
            EngineeringUnits cPitch = default(EngineeringUnits);
            EngineeringUnits peakHeight = default(EngineeringUnits);
            EngineeringUnits maxHeight = default(EngineeringUnits);
            EngineeringUnits aWallHeight = default(EngineeringUnits);
            EngineeringUnits cWallHeight = default(EngineeringUnits);
            EngineeringUnits asoffitHeight = default(EngineeringUnits);
            EngineeringUnits csoffitHeight = default(EngineeringUnits);
            EngineeringUnits dripEdgeHeight = default(EngineeringUnits);
            List<double> pitchList = default(List<double>);
            List<double> soffitWallHeight = default(List<double>);
            List<double> soffitHeight = default(List<double>);
            string endcut = SunroomViewModel.SetEndcutString(ca_endcut1, ca_endcut2, ca_endcut3);
            double roofArea = default(double);
            double ceilingPanels = default(double);
            // Overhang
            try 
            {
                overhang = new EngineeringUnits(Methods.assume_units(ca_overhang_editbox.Text, "\""), "length"); 
                initialConditions[0] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("Overhang Error", e.Message);}
            // A wall
            try 
            {
                aWall = new EngineeringUnits(Methods.assume_units(ca_awall_editbox.Text, "\""), "length");
                initialConditions[1] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("A Wall Error", e.Message);}
            // B Wall
            try 
            {
                bWall = new EngineeringUnits(Methods.assume_units(ca_bwall_editbox.Text, "\""), "length"); 
                initialConditions[2] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("B Wall Error", e.Message);}
            // C Wall
            try 
            {
                cWall = new EngineeringUnits(Methods.assume_units(ca_cwall_editbox.Text, "\""), "length");
                initialConditions[3] = true;
            }
            catch (Exception e) {SunroomViewModel.SunroomMessageBox("C Wall Error", e.Message);}
            // Thickness combobox item
            ComboItem = (ThicknessModel)ca_thick_combo.SelectedItem;
            // Create Sunroom Object
            if (initialConditions[0] && initialConditions[1] && initialConditions[2] && initialConditions[3])
            {
                Cathedral cathedral = new Cathedral(overhang.base_measure, aWall.base_measure, bWall.base_measure, 
                cWall.base_measure, ComboItem.comboValue, endcut);
                if ((bool)ca_scenario1.IsChecked) // wall height and pitch
                {
                    try
                    {
                        pitchList = new List<double>();
                        if ((bool)ca_a_ratio_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(Methods.assume_units(ca_a_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_a_angle_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(ca_a_pitch_editbox.Text, "angle");}
                        if ((bool)ca_c_ratio_radio.IsChecked) 
                        {cPitch = new EngineeringUnits(Methods.assume_units(ca_c_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_c_angle_radio.IsChecked) 
                        {cPitch = new EngineeringUnits(ca_c_pitch_editbox.Text, "angle");}
                        pitchList.Add(Methods.pitch_input(aPitch));
                        pitchList.Add(Methods.pitch_input(cPitch));
                        try
                        {
                            soffitWallHeight = new List<double>();
                            aWallHeight = new EngineeringUnits(Methods.assume_units(ca_awallheight_editbox.Text, "\""), "length");
                            cWallHeight = new EngineeringUnits(Methods.assume_units(ca_cwallheight_editbox.Text, "\""), "length");
                            soffitWallHeight.Add(aWallHeight.base_measure);
                            soffitWallHeight.Add(cWallHeight.base_measure);
                            cathedral.wall_height_pitch(pitchList, soffitWallHeight);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("A/C Wall Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                if ((bool)ca_scenario2.IsChecked) // wall and peak height
                {
                    try
                    {
                        soffitWallHeight = new List<double>();
                        aWallHeight = new EngineeringUnits(Methods.assume_units(ca_awallheight_editbox.Text, "\""), "length");
                        cWallHeight = new EngineeringUnits(Methods.assume_units(ca_cwallheight_editbox.Text, "\""), "length");
                        soffitWallHeight.Add(aWallHeight.base_measure);
                        soffitWallHeight.Add(cWallHeight.base_measure);
                        try
                        {
                            peakHeight = new EngineeringUnits(Methods.assume_units(ca_peak_editbox.Text, "\""), "length");
                            cathedral.wall_height_peak_height(soffitWallHeight, peakHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Peak Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("A/C Wall Height Error", e.Message);}
                }
                if ((bool)ca_scenario3.IsChecked) // max height and pitch
                {
                    try
                    {
                        pitchList = new List<double>();
                        if ((bool)ca_a_ratio_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(Methods.assume_units(ca_a_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_a_angle_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(ca_a_pitch_editbox.Text, "angle");}
                        if ((bool)ca_c_ratio_radio.IsChecked) 
                        {cPitch = new EngineeringUnits(Methods.assume_units(ca_c_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_c_angle_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(ca_c_pitch_editbox.Text, "angle");}
                        pitchList.Add(Methods.pitch_input(aPitch));
                        pitchList.Add(Methods.pitch_input(cPitch));
                        try
                        {
                            maxHeight = new EngineeringUnits(Methods.assume_units(ca_max_editbox.Text, "\""), "length");
                            cathedral.max_height_pitch(pitchList, maxHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Max Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                if ((bool)ca_scenario4.IsChecked) // soffit and peak height
                {
                    try
                    {
                        soffitHeight = new List<double>();
                        asoffitHeight = new EngineeringUnits(Methods.assume_units(ca_asoffit_editbox.Text, "\""), "length");
                        csoffitHeight = new EngineeringUnits(Methods.assume_units(ca_csoffit_editbox.Text, "\""), "length");
                        soffitHeight.Add(asoffitHeight.base_measure);
                        soffitHeight.Add(csoffitHeight.base_measure);
                        try
                        {
                            peakHeight = new EngineeringUnits(Methods.assume_units(ca_peak_editbox.Text, "\""), "length");
                            cathedral.soffit_height_peak_height(soffitHeight, peakHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Peak Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Soffit Height Error", e.Message);}
                }
                if ((bool)ca_scenario5.IsChecked) // soffit height and pitch
                {
                    try
                    {
                        pitchList = new List<double>();
                        if ((bool)ca_a_ratio_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(Methods.assume_units(ca_a_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_a_angle_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(ca_a_pitch_editbox.Text, "angle");}
                        if ((bool)ca_c_ratio_radio.IsChecked) 
                        {cPitch = new EngineeringUnits(Methods.assume_units(ca_c_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_c_angle_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(ca_c_pitch_editbox.Text, "angle");}
                        pitchList.Add(Methods.pitch_input(aPitch));
                        pitchList.Add(Methods.pitch_input(cPitch));
                        try
                        {
                            soffitHeight = new List<double>();
                            asoffitHeight = new EngineeringUnits(Methods.assume_units(ca_asoffit_editbox.Text, "\""), "length");
                            csoffitHeight = new EngineeringUnits(Methods.assume_units(ca_csoffit_editbox.Text, "\""), "length");
                            soffitHeight.Add(asoffitHeight.base_measure);
                            soffitHeight.Add(csoffitHeight.base_measure);
                            cathedral.soffit_height_pitch(soffitHeight, pitchList);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Soffit Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                if ((bool)ca_scenario6.IsChecked) // drip edge and peak height
                {
                    try
                    {
                        dripEdgeHeight = new EngineeringUnits(Methods.assume_units(ca_drip_editbox.Text, "\""), "length");
                        try
                        {
                            peakHeight = new EngineeringUnits(Methods.assume_units(ca_peak_editbox.Text, "\""), "length");
                            cathedral.drip_edge_peak_height(dripEdgeHeight.base_measure, peakHeight.base_measure);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Peak Height Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Drip Edge Error", e.Message);}
                }
                if ((bool)ca_scenario7.IsChecked) // drip edge and pitch
                {
                    try
                    {
                        pitchList = new List<double>();
                        if ((bool)ca_a_ratio_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(Methods.assume_units(ca_a_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_a_angle_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(ca_a_pitch_editbox.Text, "angle");}
                        if ((bool)ca_c_ratio_radio.IsChecked) 
                        {cPitch = new EngineeringUnits(Methods.assume_units(ca_c_pitch_editbox.Text, "\""), "length");}
                        if ((bool)ca_c_angle_radio.IsChecked) 
                        {aPitch = new EngineeringUnits(ca_c_pitch_editbox.Text, "angle");}
                        pitchList.Add(Methods.pitch_input(aPitch));
                        pitchList.Add(Methods.pitch_input(cPitch));
                        try
                        {
                            dripEdgeHeight = new EngineeringUnits(Methods.assume_units(ca_drip_editbox.Text, "\""), "length");
                            cathedral.drip_edge_pitch(dripEdgeHeight.base_measure, pitchList);
                        }
                        catch (Exception e) {SunroomViewModel.SunroomMessageBox("Drip Edge Error", e.Message);}
                    }
                    catch (Exception e) {SunroomViewModel.SunroomMessageBox("Pitch Error", e.Message);}
                }
                roofArea = Math.Ceiling(((double)cathedral.a_roof_panel_dict["Roof Area"] + (double)cathedral.c_roof_panel_dict["Roof Area"]) / 144.0);
                ceilingPanels = ((double)cathedral.a_armstrong_panels + (double)cathedral.c_armstrong_panels);
                List<bool> aFasciaCheck = (List<bool>)cathedral.a_fascia_dict["Fascia Check"];
                List<bool> cFasciaCheck = (List<bool>)cathedral.c_fascia_dict["Fascia Check"];
                ca_aSidePitchResult.Text = Methods.pitch_estimate(12 * Math.Tan(cathedral.a_pitch)).ToString() + "/12";
                ca_cSidePitchResult.Text = Methods.pitch_estimate(12 * Math.Tan(cathedral.c_pitch)).ToString() + "/12";
                ca_peakHeightResult.Text = Methods.sixteenth(cathedral.f_peak).ToString() + " in.";
                ca_aSoffitHeightResult.Text = Methods.sixteenth(cathedral.a_soffit).ToString() + " in.";
                ca_cSoffitHeightResult.Text = Methods.sixteenth(cathedral.c_soffit).ToString() + " in.";
                ca_aDripResult.Text = Methods.sixteenth(cathedral.a_drip_edge).ToString() + " in.";
                ca_cDripResult.Text = Methods.sixteenth(cathedral.c_drip_edge).ToString() + " in.";
                ca_maxResult.Text = Methods.sixteenth(cathedral.max_h).ToString() + " in.";
                ca_aWallResult.Text = Methods.sixteenth(cathedral.a_unpitched_wall_h).ToString() + " in.";
                ca_cWallResult.Text = Methods.sixteenth(cathedral.c_unpitched_wall_h).ToString() + " in.";
                ca_aPanelsResult.Text = cathedral.a_roof_panel_dict["Roof Panels"].ToString();
                ca_aPanelLengthResult.Text = cathedral.a_panel_length_dict["Panel Length"].ToString() + " in.";
                ca_cPanelsResult.Text = cathedral.c_roof_panel_dict["Roof Panels"].ToString();
                ca_cPanelLengthResult.Text = cathedral.c_panel_length_dict["Panel Length"].ToString() + " in.";
                ca_totalRoofResult.Text = Math.Round(roofArea, 0).ToString() + " sq.ft.";
                ca_totalPanelsResult.Text = ((double)cathedral.a_roof_panel_dict["Roof Panels"] + (double)cathedral.c_roof_panel_dict["Roof Panels"]).ToString();
                ca_ceilingPanelsResult.Text = ceilingPanels.ToString();
                ca_aOverhangResult.Text = Methods.sixteenth(cathedral.overhang).ToString() + " in.";
                ca_cOverhangResult.Text = Methods.sixteenth(cathedral.overhang).ToString() + " in.";
                ca_bOverhangResult.Text = Methods.sixteenth(cathedral.side_overhang).ToString() + " in.";
                if ((bool)cathedral.a_hang_rail_dict["Hang Rail Check"])
                {
                    ca_aHangRailsResult.Text = "There are 2 pairs of\nhang rails at " + cathedral.a_hang_rail_dict["Hang Rail"].ToString() + " in.";
                }
                else
                {
                    ca_aHangRailsResult.Text = "There is 1 pair of \nhang rails at " + cathedral.a_hang_rail_dict["Hang Rail"].ToString() + " in.";
                }
                if ((bool)cathedral.c_hang_rail_dict["Hang Rail Check"])
                {
                    ca_cHangRailsResult.Text = "There are 2 pairs of\nhang rails at " + cathedral.c_hang_rail_dict["Hang Rail"].ToString() + " in.";
                }
                else
                {
                    ca_cHangRailsResult.Text = "There is 1 pair of \nhang rails at " + cathedral.c_hang_rail_dict["Hang Rail"].ToString() + " in.";
                }
                if (aFasciaCheck[0])
                {
                    ca_aFasciaResult.Text = "There are 2 pieces of\nfascia at " + cathedral.a_fascia_dict["Wall Fascia"].ToString() + " in.\nfor the B Wall.";
                }
                else
                {
                    ca_aFasciaResult.Text = "There is 1 piece of\nfascia at " + cathedral.a_fascia_dict["Wall Fascia"].ToString() + " in.\nfor the B Wall.";
                }
                if (aFasciaCheck[1])
                {
                    ca_aFasciaResult2.Text = "There are 2 pieces of\nfascia at " + cathedral.a_fascia_dict["Side Fascia"].ToString() + " in.\nfor the A & C Walls.";
                }
                else
                {
                    ca_aFasciaResult2.Text = "There is 1 piece of\nfascia at " + cathedral.a_fascia_dict["Side Fascia"].ToString() + " in.\nfor the A & C Walls.";
                }
                if (cFasciaCheck[0])
                {
                    ca_cFasciaResult.Text = "There are 2 pieces of\nfascia at " + cathedral.c_fascia_dict["Wall Fascia"].ToString() + " in.\nfor the B Wall.";
                }
                else
                {
                    ca_cFasciaResult.Text = "There is 1 piece of\nfascia at " + cathedral.c_fascia_dict["Wall Fascia"].ToString() + " in.\nfor the B Wall.";
                }
                if (cFasciaCheck[1])
                {
                    ca_cFasciaResult2.Text = "There are 2 pieces of\nfascia at " + cathedral.c_fascia_dict["Side Fascia"].ToString() + " in.\nfor the A & C Walls.";
                }
                else
                {
                    ca_cFasciaResult2.Text = "There is 1 piece of\nfascia at " + cathedral.c_fascia_dict["Side Fascia"].ToString() + " in.\nfor the A & C Walls.";
                }
            }
            else {SunroomViewModel.SunroomMessageBox("Calculation Error", "Missing Inputs");}
        }
    }
}