using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using System.Reactive;
using System.Reactive.Linq;
using System.Collections.Generic;
using LivingspaceToolkit.Models;
using LivingspaceToolkit.ViewModels;

namespace Sunroom.Views
{
    class StudioViewModel : ViewModelBase
    {
        public string stPitchLabel = "/12 in.";
        public List<ThicknessModel> stThickness;
        ThicknessCombo tc = new ThicknessCombo();
        public ReactiveCommand<Unit, string> stRatioRadio {get;}
        public ReactiveCommand<Unit, string> stAngleRadio {get;}
        public ReactiveCommand<Unit, Unit> stEcoGreen {get;}
        public ReactiveCommand<Unit, Unit> stAluminum {get;}
        public ReactiveCommand<Unit, List<ThicknessModel>> stEcoGreenThickness {get;}
        public ReactiveCommand<Unit, List<ThicknessModel>> stAluminumThickness {get;}
        public ReactiveCommand<Unit, Unit> stScenario { get; set; }
        public ReactiveCommand<Unit, Unit> stEndcuts { get; set; }
        public ReactiveCommand<Unit, Unit> stEndcutFascia { get; set; }
        public ReactiveCommand<Unit, Unit> StButtonClick {get;}
        public StudioViewModel()
        {
            stRatioRadio = ReactiveCommand.Create(() => StPitchLabel = "/12 in.");
            stAngleRadio = ReactiveCommand.Create(() => StPitchLabel = "deg");
            stEcoGreenThickness = ReactiveCommand.Create(() => StThickness = (List<ThicknessModel>)(tc.thicknessCreate(ThicknessModel.eGreen)));
            stAluminumThickness = ReactiveCommand.Create(() => StThickness = (List<ThicknessModel>)(tc.thicknessCreate(ThicknessModel.aluminum)));
            stEcoGreen = ReactiveCommand.CreateFromObservable(() => stEcoGreenThickness.Execute()
                                                                .SelectMany(_ => stEndcuts.Execute()));
            stAluminum = ReactiveCommand.CreateFromObservable(() => stAluminumThickness.Execute()
                                                                .SelectMany(_ => stEndcuts.Execute()));
            stScenario = ReactiveCommand.Create(() => {StudioView.Scenarios(); return Unit.Default;});
            stEndcuts = ReactiveCommand.Create(() => {StudioView.Endcuts(); return Unit.Default;});
            stEndcutFascia = ReactiveCommand.Create(() => {StudioView.EndcutFascia();});
            StButtonClick = ReactiveCommand.Create(() => {StudioView.stButton();});
        }
        public string StPitchLabel
        {
            get => stPitchLabel;
            set => this.RaiseAndSetIfChanged(ref stPitchLabel, value);
        }
        public List<ThicknessModel> StThickness
        {
            get => stThickness;
            set => this.RaiseAndSetIfChanged(ref stThickness, value);
        }
    }
}