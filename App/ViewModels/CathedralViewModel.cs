using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using ReactiveUI;
using System.Reactive;
using System.Reactive.Linq;
using System.Collections.Generic;
using LivingspaceToolkit.Models;
using LivingspaceToolkit.ViewModels;

namespace Sunroom.Views
{
    class CathedralViewModel : ViewModelBase
    {
        public string caAPitchLabel = "/12 in.";
        public string caCPitchLabel = "/12 in.";
        public List<ThicknessModel> caThickness;
        ThicknessCombo tc = new ThicknessCombo();
        public ReactiveCommand<Unit, string> caARatioRadio {get;}
        public ReactiveCommand<Unit, string> caAAngleRadio {get;}
        public ReactiveCommand<Unit, string> caCRatioRadio {get;}
        public ReactiveCommand<Unit, string> caCAngleRadio {get;}
        public ReactiveCommand<Unit, Unit> caEcoGreen {get;}
        public ReactiveCommand<Unit, Unit> caAluminum {get;}
        public ReactiveCommand<Unit, List<ThicknessModel>> caEcoGreenThickness {get;}
        public ReactiveCommand<Unit, List<ThicknessModel>> caAluminumThickness {get;}
        public ReactiveCommand<Unit, Unit> caScenario { get; set; }
        public ReactiveCommand<Unit, Unit> caEndcuts { get; set; }
        public ReactiveCommand<Unit, Unit> caEndcutFascia { get; set; }
        public ReactiveCommand<Unit, Unit> CaButtonClick {get;}
        public CathedralViewModel()
        {
            caARatioRadio = ReactiveCommand.Create(() => CaAPitchLabel = "/12 in.");
            caAAngleRadio = ReactiveCommand.Create(() => CaAPitchLabel = "deg");
            caCRatioRadio = ReactiveCommand.Create(() => CaCPitchLabel = "/12 in.");
            caCAngleRadio = ReactiveCommand.Create(() => CaCPitchLabel = "deg");
            caEcoGreenThickness = ReactiveCommand.Create(() => CaThickness = (List<ThicknessModel>)(tc.thicknessCreate(ThicknessModel.eGreen)));
            caAluminumThickness = ReactiveCommand.Create(() => CaThickness = (List<ThicknessModel>)(tc.thicknessCreate(ThicknessModel.aluminum)));
            caEcoGreen = ReactiveCommand.CreateFromObservable(() => caEcoGreenThickness.Execute()
                                                                .SelectMany(_ => caEndcuts.Execute()));
            caAluminum = ReactiveCommand.CreateFromObservable(() => caAluminumThickness.Execute()
                                                                .SelectMany(_ => caEndcuts.Execute()));
            caScenario = ReactiveCommand.Create(() => {CathedralView.Scenarios(); return Unit.Default;});
            caEndcuts = ReactiveCommand.Create(() => {CathedralView.Endcuts(); return Unit.Default;});
            caEndcutFascia = ReactiveCommand.Create(() => {CathedralView.EndcutFascia();});
            CaButtonClick = ReactiveCommand.Create(() => {CathedralView.caButton();});
        }
        public string CaAPitchLabel
        {
            get => caAPitchLabel;
            set => this.RaiseAndSetIfChanged(ref caAPitchLabel, value);
        }
        public string CaCPitchLabel
        {
            get => caCPitchLabel;
            set => this.RaiseAndSetIfChanged(ref caCPitchLabel, value);
        }
        public List<ThicknessModel> CaThickness
        {
            get => caThickness;
            set => this.RaiseAndSetIfChanged(ref caThickness, value);
        }
    }
}